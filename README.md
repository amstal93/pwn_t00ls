
> For pentesters allergic to virtual machines 

Includes some of the tools missing from other amazing images such as [parrotsec/security](https://hub.docker.com/r/parrotsec/security).

## Usage

**display help**
```bash
docker run zar3bski/pwn_t00ls
```

**use a tool**
```bash
docker run zar3bski/pwn_t00ls [tool name] [argument]
```

**use host resources** (like dictionaries) 
```bash
docker run -v $PWD/some_dict:/usr/share/dict/some_dict:ro zar3bski/pwn_t00ls
```
## Includes

|           name |  version   | sources                                                                                            | description                                  |
| -------------: | :--------: | :------------------------------------------------------------------------------------------------- | :------------------------------------------- |
|    `dotdotpwn` |   3.0.2    | [https://github.com/wireghoul/dotdotpwn](https://github.com/wireghoul/dotdotpwn)                   | Directory traversal fuzzer for HTTP/FTP/TFTP |
| `searchsploit` | 2020-11-04 | [https://github.com/offensive-security/exploitdb](https://github.com/offensive-security/exploitdb) | Search engine for ExploitDB                  |
|        `wfuzz` |   3.1.0    | [https://github.com/xmendez/wfuzz](https://github.com/xmendez/wfuzz)                               | Web application fuzzer                       |