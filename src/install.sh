#!/usr/bin/env ash

# Create temporary folder
mkdir /tmp/buid && cd /tmp/buid

# Install DotDotPwn
yes | perl -MCPAN -e "install TFTP"

wget https://github.com/wireghoul/dotdotpwn/archive/3.0.2.zip
unzip 3.0.2.zip

cp -r dotdotpwn-3.0.2/DotDotPwn/ /usr/lib/perl5/vendor_perl
cp dotdotpwn-3.0.2/dotdotpwn.pl /usr/bin/dotdotpwn

# Install Searchsploit

wget https://raw.githubusercontent.com/offensive-security/exploitdb/2020-11-04/searchsploit
chmod +x searchsploit
sed -i 's/rc_file=""/rc_file="\/etc\/searchsploit"/g' searchsploit
cp searchsploit /usr/bin/searchsploit

touch /etc/searchsploit
echo '#searchsploit conf file' > /etc/searchsploit

# Install wfuzz

pip3 install wfuzz==3.1.0


# Remove temporary folder
rm -rf /tmp/buid
